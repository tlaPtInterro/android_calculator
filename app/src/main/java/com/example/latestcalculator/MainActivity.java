package com.example.latestcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    private EditText textOperations ; // retrieve the text from the attribut
    private String currentInput = "";
    private EditText textResultats ; // retrieve the text from the attribut
    private boolean anotherOp;

    // Créer un nouveau Handler pour gérer les opérations de calcul dans un thread séparé
    private Handler calculationHandler = new Handler(new Handler.Callback() {
        public boolean handleMessage(Message msg) {
            // Récupérer le résultat du calcul à partir du message
            String resultString = (String) msg.obj;
            // Mettre à jour l'interface utilisateur avec le résultat du calcul
            textResultats.setText(resultString);
            return true;
        }
    });


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textOperations = findViewById(R.id.editText1);
        textResultats = findViewById(R.id.editText2);
        anotherOp = false;

        // creation bouton "="
        Button buttonEquals = new Button(this);
        buttonEquals.setText("=");
        // Trouver le conteneur LinearLayout spécifié dans le fichier XML
        LinearLayout linearLayout = findViewById(R.id.sectionResult);
        // Ajouter le bouton "=" au conteneur LinearLayout
        linearLayout.addView(buttonEquals);
    }

    public void myClickHandler (View view) {

        if (anotherOp) {
            // make empty fields at first operation
            textOperations.setText("");
            textResultats.setText("");
        }

        // on verifie quelle boutton à été cliqué la dessus
        if (view.getId() == R.id.buttonZero) {
            currentInput += "0";
        }else if (view.getId() == R.id.buttonOne) {
            currentInput += "1";
        } else if (view.getId() == R.id.buttonTwo) {
            currentInput += "2";
        } else if (view.getId() == R.id.buttonThree) {
            currentInput += "3";
        } else if (view.getId() == R.id.buttonFour) {
            currentInput += "4";
        }else if (view.getId() == R.id.buttonFive) {
            currentInput += "5";
        }else if (view.getId() == R.id.buttonSix) {
            currentInput += "6";
        }else if (view.getId() == R.id.buttonSeven) {
            currentInput += "7";
        }else if (view.getId() == R.id.buttonEight) {
            currentInput += "8";
        }else if (view.getId() == R.id.buttonNine) {
            currentInput += "9";
        }else if (view.getId() == R.id.buttonAdd) {
            currentInput += "+";
        }else if (view.getId() == R.id.buttonSubs) {
            currentInput += "-";
        }else if (view.getId() == R.id.buttonDivide) {
            currentInput += "/";
        }else if (view.getId() == R.id.buttonMult) {
            currentInput += "*";

        // TODO suppr ca et l'ajouter sur notre boutton 
        }else if (view.getId() == R.id.buttonResult) {
            calcul(currentInput);
            currentInput="";
        }

        if (view.getId() == R.id.buttonResult) {
            // Lancement du calcul dans un thread séparé
            new Thread(new Runnable() {
                @Override
                public void run() {
                    calcul(currentInput);
                }
            }).start();
            currentInput = "";
        }

        textOperations.setText(currentInput);
    }

    public void calcul(String currentInput) {

        // Utilisation de split pour extraire les chiffres et l'opérateur
        String[] parts = currentInput.split("\\s*[-+*/]\\s*");

        // Vérification du résultat
        String resultString = "";
        if (parts.length == 2) {
            String operand1 = parts[0]; // on transforme ces string en chiffre
            String operand2 = parts[1];

            // extraction de l'opérateur
            String operator = currentInput.replaceAll("[0-9]", "").trim();

            int num1 = Integer.parseInt(operand1);
            int num2 = Integer.parseInt(operand2);

            int result = 0;
            anotherOp = true;

            switch (operator) {
                case "+":
                    result = num1 + num2;
                    resultString = String.valueOf(result);
                    textResultats.setText(resultString);
                    break;
                case "-":
                    result = num1 - num2;
                    resultString = String.valueOf(result);
                    textResultats.setText(resultString);
                    break;

                case "*":
                    result = num1 * num2;
                    resultString = String.valueOf(result);
                    textResultats.setText(resultString);
                    break;

                case "/":
                    // Assurez-vous que le diviseur n'est pas zéro pour éviter une division par zéro
                    if (num2 != 0) {
                        result = num1 / num2;
                        resultString = String.valueOf(result);
                        textResultats.setText(resultString);
                    } else {
                        // Gestion de la division par zéro (à votre discrétion)
                        System.out.println("Erreur : Division par zéro");
                        resultString = "Division impossible par 0";
                        textResultats.setText(resultString);
                    }
                    break;
                default:
                    // Gestion d'un opérateur non pris en charge (à votre discrétion)
                    System.out.println();
                    resultString = "Erreur : Opérateur non pris en charge";
                    break;
            }

            // Envoyer le résultat au Handler pour mettre à jour l'interface utilisateur
            Message message = calculationHandler.obtainMessage();
            message.obj = resultString;
            calculationHandler.sendMessage(message);
        }
    }


}